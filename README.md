# CI Builds

Daily at noon Central

# Add repo to your apt sources.list

```
deb [trusted=yes] https://beagleboard.beagleboard.io/ti-rt-linux-kernel-6.1.y-arm64 stable main
```

# Quick One line:

```
sudo sh -c "echo 'deb [trusted=yes] https://beagleboard.beagleboard.io/ti-rt-linux-kernel-6.1.y-arm64 stable main' > /etc/apt/sources.list.d/ti-rt-linux-kernel-6.1.y-arm64.list"
```

#
